# %%
# Import packages
from scipy.io import loadmat
import numpy as np
import os
from glob import glob

# %%
# Function definitions
def save_mat_to_csv(math_path, headers):
    """
    This function will load a mat file and convert it to csv with the correct column names and order for Coloc Tesseler import
    """
    mat = loadmat(math_path)
    mat_array = mat[list(mat.keys())[-1]]
    new_array = mat_array[:, [3,4,5,6,1,7,8]]
    path = math_path[:math_path.rfind('/')+1]
    mat_name = math_path.split('/')[-1].replace('.mat','.csv')
    np.savetxt(os.path.join(path,mat_name), new_array, comments='',delimiter=',', header=headers)

def main():
    """
    This is the actual batch processing of all mat files in root folder and subfolders
    """
    root = "./"
    all_paths = [path for path in glob(root+"*/*/*") if os.path.isdir(path)]
    headers = "xnm,ynm,znm,intensity,frame,sigmaX,sigmaY"

    for path in all_paths:
        mats = [os.path.join(path,file) for file in os.listdir(path) if file.endswith('.mat')]

        for mat in mats:
            csv = mat.replace('.mat', '.csv')
            if os.path.exists(csv):
                print('The csv of', mat, 'already exists')
            else:
                print('Processing', mat)
                save_mat_to_csv(mat, headers)
# %%
# Start of program
if __name__ == "__main__":
    main()